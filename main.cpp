//*************************************//
// ECE 373 - Project 2 Main Executable //
// Hearts Game			       //
//				       //
// Authors: Michael Buciuman-Coman     //
//	    Avetis Shahverdian	       //
//          Scott Clark     	       //
//                                     //
// Date: 10/24/2014                    //
//*************************************//

#include "game.hpp"

int main()
{
	Game* game = new Game();
	game->startGame();
	game->endGame();
	return 0;
}
