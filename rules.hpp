#include "player.hpp"
#include <string>

class Trick
{
protected:
	bool heartsBroken;		//have hearts been broken?
	bool twoPlayed;			//has the two of clubs been played?
	bool firstTrick;		//is this the first trick?
	int trickTaker;			//index of the trick taker
	int trick[4]; 			//array containing current cards in trick (0 = no card)
	int currentTrickCard; 		//index of next card to be played
	void addToTrick(int*); 		//adds card to trick
	void setSuit(int*); 		//sets the suit of the trick
public:
	Trick(void); 			//constructor
	~Trick(); 			//deconstructor
	int suit; 			//suit of trick (0 = clubs, 1 = diamonds, 2 = spades, 3 = hearts)
	int* getTrick(void); 		//returns trick array
	void printTrick(void); 		//prints cards in trick on one line
	void resetTrick(void); 		//sets all cards in trick to 0
	bool getHearts(void);		//gets if hearts is broken
	void resetHearts(void);		//resets hearts being broken
	void resetTwoPlayed(void);	//resets two being played
	void setFirstTrick(bool);	//sets if first trick
	int* scoreTrick(void); 		//scores cards in trick (called after 4 cards have been played)
	void setTrickTaker(int);	//sets the index of the trick taker;
	int getTrickTaker(void);	//returns the index of the trick taker;
	bool testSuit(int*); 		//tests the card to see if it matches the suit
	int getSuit(int*); 		//returns the suit of the card in the parameters
};

class Rules : public Trick
{
public:
	Rules(void); 				//constructor
	~Rules(); 				//deconstructor
	std::string testCard(Player*, int*);	//tests card to see if it can be played in trick
	void playCard(int*); 			//adds card to trick
};
