class Deck
{
private:
	int deck[52]; 		//deck of cards
public:
	Deck(); 		//constructor
	~Deck();	 	//deconstructor
	int* getDeck(); 	//returns pointer to deck array
	void swap(int*, int*); 	//swaps two cards in deck
	void newOrderedDeck(); 	//creates a new deck with all cards in order
	void newShuffle(); 	//shuffles the deck
	void garbageCollect(); 	//cleans up variables
};
