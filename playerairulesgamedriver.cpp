// Player, AI, Rule and Game Driver Class

#include <iostream>
#include "game.hpp"

#define _DEBUG 4 //for testing

int main()
{
	int testHand[] = {2,4,5,1,29,22,3,6,30,24,26,42,15};
	Player* player = new Player(testHand);

	int testHand2[] = {2,4,5,1,29,22,3,6,30,24,26,42,15};
	AI* ai = new AI(testHand2);

	Rules* rules = new Rules();
	Game* game = new Game();

	if(_DEBUG == 1)
	{
		player->printHand();
		player->resetHand();
		player->printHand();
		player->setHand(testHand);
		player->printHand();
		player->playCard(3);
		player->printHand();
		player->resetRoundScore();
		std::cout << player->getRoundScore() << std::endl;
		player->addRoundScore(15);
		std::cout << player->getRoundScore() << std::endl << std::endl;
	}

	if(_DEBUG == 2)
	{
		ai->printHand();
		ai->resetHand();
		ai->printHand();
		ai->setHand(testHand2);
		ai->printHand();
		ai->playCard(3);
		ai->printHand();
		ai->resetRoundScore();
		std::cout << ai->getRoundScore() << std::endl;
		ai->addRoundScore(15);
		std::cout << ai->getRoundScore() << std::endl;
		ai->setVariables(3, 2);
		std::cout << ai->chooseCard() << std::endl;
		ai->printHand();
	}

	if(_DEBUG == 3)
	{
		rules->printTrick();

		int card = 25;
		rules->playCard(&card);
		card = 37;
		rules->playCard(&card);
		card = 16;
		rules->playCard(&card);
		card = 43;
		rules->playCard(&card);
		rules->printTrick();

		card = 32;
		std::cout << rules->getSuit(&card) << std::endl;
		card = 14;
		std::cout << rules->testCard(&card) << std::endl;
		card = 50;
		std::cout << rules->testCard(&card) << std::endl;

		game->addToScores(rules->scoreTrick(0));
		game->printScores();

		rules->resetTrick();
		rules->printTrick();

		int testScores[] = {0, 0, 26, 0};
		game->addToScores(testScores);
		game->printScores();

		game->resetScores();
		game->printScores();
	}
	
	if(_DEBUG == 4)
	{
		game->startRound();
		int* testTrick = new int;
		for(int i = 0; i < 4; i++)
		{
			*testTrick = (i+1)*11;
			game->rules->playCard(testTrick);
		}
		while(true)
		{
			game->displayCards();
			game->getInput();
		}
	}
}
