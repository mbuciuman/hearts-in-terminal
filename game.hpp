#include "rules.hpp"
#include "deck.hpp"
#include "display.h"
#include <string>

class Game
{
private:
	int passDirection;			//direction which players pass at the start of the round (0 = left, 1 = right, 2 = across, 3 = no passing)
	int leadPlayer; 			//player who picks first card to play in trick
	int scores[4]; 				//current scores of the 4 players
	display* gameDisplay;			//Display
	Deck* deck;				//The game deck
	Player* players[4];			//Players (0 = Player, 1 = Left AI, 2 = Across AI, 3 = Right AI)
	bool playGame(void);			//Plays a round and returns false if game is over
	void determineStartingLead(void);	//Sets lead player
	void passCards(void);			//Players pass cards
	void resolveTrick(void);		//Play trick
	int* adjustCardForDisplay(int);		//Returns suit and card number in order for card displayed properly in the display class
	string* cardData(int);			//Returns string array with card suit and number
	void endRound(void);			//Clean up round
	int checkMoon(int[4]); 			//checks to see if any player shot the moon and returns index of player (-1 = no player shot the moon)
	bool checkScores(void);			//checks to see if any player's score is above 100
	int* getScores(void); 			//returns scores of players
public:
	Game(void);				//Game constructor
	~Game();				//Game deconstructor
	Rules* rules;				//Rule checker
	void startGame(void);			//Start game
	void endGame(void);			//Clean variables
	void startRound(void);			//Set up round
	int getInput(void);			//Polls input for valid input and returns index of card
	void refreshDisplay();			//Clears display
	void displayScores();			//Displays scores in top banner
	void clearTopBanner();			//Clears top banner
	void displayInTopBanner(string);	//Displays string in top banner
	void clearBottomBanner();		//Clears bottom banner
	void displayInBottomBanner(string);	//Displays string in bottom banner
	void displayCards(void);		//Display cards on screen
	void addToScores(int[4]); 		//adds input array of round scores to total scores
	void printScores(void); 		//prints cards in trick on one line
	void resetScores(void); 		//sets total scores of players to 0
};
