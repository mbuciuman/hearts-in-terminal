# Project 2 Makefile

CC=g++
DEBUG=
CFLAGS=-c -Wall $(DEBUG)
LDFLAGS=-lncursesw $(DEBUG)
SOURCES=main.cpp ai.cpp deck.cpp display.cpp game.cpp player.cpp rules.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=project2


all: clean $(SOURCES) $(EXECUTABLE)

debug: clean
debug: DEBUG +=-g
debug: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

main.o : game.hpp
ai.o : player.hpp
deck.o : deck.hpp
display.o : display.h
game.o : deck.hpp display.h game.hpp player.hpp rules.hpp rules.hpp
player.o: player.hpp
rules.o : rules.hpp player.hpp

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf *.o *.gch *~ $(EXECUTABLE)

