// Player Class

#include <iostream>
#include <cstring>
#include <cstring>
#include "player.hpp"

Hand::Hand()
{
	resetHand();
}

Hand::Hand(int h[13])
{
	memcpy(hand, h, sizeof(hand));
	sortHand();
}

Hand::~Hand()
{
	std::cout << "Hand::~Hand" << std::endl;
}

void Hand::sortHand()
{
	int lowestIndex = 0, lowestCard = 0;
	for(int i = 0; i < 13; i++)
	{
		lowestIndex = i;
		lowestCard = hand[i];
		for(int j = i + 1; j < 13; j++)
		{
			if(lowestCard > hand[j])
			{
				lowestCard = hand[j];
				lowestIndex = j;
			}
		}
		swap(&hand[i], &hand[lowestIndex]);
	}
}

int* Hand::getHand()
{
	return hand;
}

void Hand::setHand(int h[13])
{
	memcpy(hand, h, sizeof(hand));
	sortHand();
}

void Hand::resetHand()
{
	int resetHand[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	memcpy(hand, resetHand, sizeof(hand));
}

void Hand::swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

void Hand::printHand()
{
	for(int i = 0; i < 13; i++)
		std::cout << hand[i] << ' ';
	std::cout << std::endl;
}

Player::Player()
: Hand()
{
	resetRoundScore();
}

Player::Player(int h[13])
: Hand(h)
{
	resetRoundScore();
}

Player::~Player()
{
	std::cout << "Player::~Player" << std::endl;
}

int Player::peekCard(int index)
{
	return getHand()[index];
}

int Player::popCard(int index)
{
	int card = getHand()[index];
	getHand()[index] = 0;
	sortHand();
	return card;
}

void Player::addRoundScore(int points)
{
	roundScore += points;
}

int Player::getRoundScore()
{
	return roundScore;
}

void Player::resetRoundScore()
{
	roundScore = 0;
}
