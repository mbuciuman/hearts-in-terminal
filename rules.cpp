// Rules Class

#include <iostream>
#include <cstring>
#include <string>
#include "rules.hpp"

Trick::Trick()
{
	suit = 0;
	heartsBroken = false;
	twoPlayed = false;
	firstTrick = true;
	resetTrick();
}

Trick::~Trick()
{
	std::cout << "Trick::~Trick" << std::endl;
}

int* Trick::getTrick()
{
	return trick;
}

void Trick::addToTrick(int *card)
{
	trick[currentTrickCard] = *card;
	currentTrickCard++;
}

void Trick::printTrick()
{
	for(int i = 0; i < 4; i++)
		std::cout << trick[i] << ' ';
	std::cout << std::endl;
}

void Trick::resetTrick()
{
	currentTrickCard = 0;
	suit = 0;
	int resetTrick[] = {0, 0, 0, 0};
	memcpy(trick, resetTrick, sizeof(trick));
}

bool Trick::getHearts()
{
	return heartsBroken;
}

void Trick::resetHearts()
{
	heartsBroken = false;
}

void Trick::resetTwoPlayed()
{
	twoPlayed = false;
}

void Trick::setFirstTrick(bool ft)
{
	firstTrick = ft;
}

int* Trick::scoreTrick()
{
	int* trickScores = new int[4];
	for(int i = 0; i < 4; i++)
		trickScores[i] = 0;
	int trickTotal = 0;
	int tTaker = 0;
	int trickLargestCard = trick[0];
	for(int i = 0; i < 4; i++)
	{
		if(getSuit(&trick[i]) == 3)
			trickTotal ++;
		else if(trick[i] == 37) //if trick card is queen of spades
			trickTotal += 13;

		if(testSuit(&trick[i]) && trick[i] > trickLargestCard)
		{
			trickLargestCard = trick[i];
			tTaker = i;
		}
	}

	for(int i = 0; i < 4; i++)
		if(i == tTaker)
			trickScores[i] = trickTotal;

	setTrickTaker(tTaker);

	return trickScores;	
}

void Trick::setTrickTaker(int tt)
{
	trickTaker = tt;
}

int Trick::getTrickTaker()
{
	return trickTaker;
}

void Trick::setSuit(int *card)
{
	suit = (*card-1)/13;
}

bool Trick::testSuit(int *card)
{
	return suit == getSuit(card);
}

int Trick::getSuit(int *card)
{
	return (*card-1)/13;
}

Rules::Rules()
: Trick()
{
}

Rules::~Rules()
{
	std::cout << "Rules::~Rules" << std::endl;
}

std::string Rules::testCard(Player *player, int *card)
{
	if(twoPlayed) //if the two of clubs was played
	{
		if(currentTrickCard == 0) //if the current card being played is the lead player's card
		{
			if(getSuit(card) == 3 && !heartsBroken) //if hearts is not broken, the player cannot play a heart
				return "Hearts not broken.";
			return "";
		}
		else
		{
			if(!testSuit(card)) //if the card played is not of the current suit
			{
				for(int i = 0; i < 13; i++) //if this does not return false, it is assumed player does not have card of matching suit in hand
					if(player->getHand()[i] != 0 && testSuit(&(player->getHand()[i])))
						return "Player has card matching suit in hand.";
				if(firstTrick) //if it is the first trick, neither queen of spades nor a heart can be played
				{
					if(getSuit(card) == 3)
						return "Hearts cannot be played on first trick.";
					if(*card == 37)
						return "Queen of spades cannot be played on first trick.";
					return "";
				}
				else
					return "";
			}
			else
				return "";
		}
	}
	else if(*card == 1) //if the two of clubs was not played and the card being played is the two of clubs
	{
		twoPlayed = true;
		return "";
	}
	else //if the two of clubs was not played and the card being played is not the two of clubs
		return "Two of clubs must be played.";
}

void Rules::playCard(int *card)
{
	int* tempCard = new int;
	*tempCard = *card;
	if(!twoPlayed)
		twoPlayed = true;
	if(currentTrickCard == 0) //if
		setSuit(card);
	if(getSuit(card) == 3) //if player played hearts card 
		heartsBroken = true;
	addToTrick(tempCard);
}
