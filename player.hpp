class Hand
{
private:
	int hand[13]; 			//player hand
public:
	Hand(void); 			//constructor
	Hand(int[13]); 			//constructor with cards
	~Hand(); 			//deconstructor
	void sortHand(void); 		//sorts hand by card number
	int* getHand(void); 		//returns pointer to start of hand array
	void setHand(int[13]); 		//sets the hand array to new hand array then sorts
	void resetHand(void); 		//sets all cards to 0, meaning no card
	void swap(int*, int*); 		//swaps two cards in hand; used in sorting
	void printHand(void); 		//prints out the values of the hand array on one line
};

class Player : public Hand
{
private:
	int roundScore; 		//current player's score for the round
public:
	Player(void); 			//constructor
	Player(int[13]); 		//constructor with cards
	~Player(); 			//deconstructor
	int peekCard(int);		//peeks at card at index and returns value
	int popCard(int); 		//pops card at index, resorts hand, and returns value
	void addRoundScore(int);	//adds int to the round's score
	int getRoundScore(void); 	//returns round's score
	void resetRoundScore(void); 	//sets round's score to 0
};

class AI : public Player
{
private:
	int turnNumber; 		//int between 0 and 3 determining which turn order the AI is
	int cardsInTrick[]; 		//cards in current trick
	int suit; 			//current trick suit (0 = clubs, 1 = diamonds, 2 = spades, 3 = hearts)
	bool shootingMoon; 		//is AI attempting to shoot the moon?
public:	
	AI(void);			//constructor
	AI(int[13]);			//constructor with cards
	~AI(); 				//deconstructor
	int chooseCard(void); 		//returns card after determining which one to play
	int lowestCardInSuit(int); 	//returns index of highest card of suit in hand (-1 if no card exists in suit) (if parameter int == -1, returns lowest card in hand)
	int highestCardInSuit(int); 	//returns index of highest card of suit in hand (-1 if no card exists in suit) (if parameter int == -1, returns highest card in hand)
	void setVariables(int, int); 	//sets private vars used in chooseCard
};
