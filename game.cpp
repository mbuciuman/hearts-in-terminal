// Game Class

#include "game.hpp"
#include <cstring>
#include <sstream>
#include <signal.h>
#include <unistd.h>
#include <ncurses.h>
#include <string>
#include <stdlib.h>

Game::Game()
{
	passDirection = 0;
	leadPlayer = 0;
	resetScores();
	gameDisplay = new display();
	deck = new Deck();
	rules = new Rules();
	players[0] = new Player();
	players[1] = new AI();
	players[2] = new AI();
	players[3] = new AI();
}

Game::~Game()
{
	passDirection = 0;
	leadPlayer = 0;
	resetScores();
	delete gameDisplay;
	delete deck;
	delete rules;
}

bool Game::playGame()
{
	displayScores();
	startRound();
	displayCards();
	passCards();
	determineStartingLead();
	for(int i = 0; i < 13; i++)
		resolveTrick();
	endRound();
	return !checkScores();
}

void Game::startRound()
{
	deck->newOrderedDeck();
	deck->newShuffle();
	for(int i = 0; i < 4; i++)
		players[i]->setHand(deck->getDeck() + i*13);
}

void Game::passCards()
{
	std::ostringstream oss;
	oss.str("NEW ROUND | ");
	switch(passDirection)
	{
		case 0:
			oss << "Passing to left.";
			break;
		case 1:
			oss << "Passing to right.";
			break;
		case 2:
			oss << "Passing across.";
			break;
		case 3:
			oss << "No passing.";
			break;
	}
	displayInBottomBanner(oss.str());

	if(passDirection != 3)
	{
		int** passedCards = new int*[4];
		for(int i = 0; i < 4; i++)
		    passedCards[i] = new int[3];
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				if(i == 0)
					passedCards[i][j] = players[0]->popCard(getInput());
				else
					passedCards[i][j] = static_cast<AI*>(players[i])->chooseCard();
				oss.str("");
				string* cardDataAndNum = cardData(passedCards[0][j]);
				switch(passDirection)
				{
					case 0:
						oss << "Card chosen to pass left: " << cardDataAndNum[1] << " of " << cardDataAndNum[0];
						break;
					case 1:
						oss << "Card chosen to pass right: " << cardDataAndNum[1] << " of " << cardDataAndNum[0];
						break;
					case 2:
						oss << "Card chosen to pass across: " << cardDataAndNum[1] << " of " << cardDataAndNum[0];
						break;
					case 3:
						oss << "ERROR::Game::passCards()";
						break;
				}
				displayInBottomBanner(oss.str());
				refreshDisplay();
				displayCards();
			}
		}
		switch(passDirection)
		{
			case 0:
				for(int i = 0; i < 4; i++)
				{
					int* tempHand = new int[13];
					for(int j = 0; j < 13; j++)
					{
						if(j < 3)
							tempHand[j] = passedCards[(i+3)%4][j];
						else
							tempHand[j] = players[i]->getHand()[j];
					}
					players[i]->setHand(tempHand);
				}
				break;
			case 1:
				for(int i = 0; i < 4; i++)
				{
					int* tempHand = new int[13];
					for(int j = 0; j < 13; j++)
					{
						if(j < 3)
							tempHand[j] = passedCards[(i+1)%4][j];
						else
							tempHand[j] = players[i]->getHand()[j];
					}
					players[i]->setHand(tempHand);
				}
				break;
			case 2:
				for(int i = 0; i < 4; i++)
				{
					int* tempHand = new int[13];
					for(int j = 0; j < 13; j++)
					{
						if(j < 3)
							tempHand[j] = passedCards[(i+2)%4][j];
						else
							tempHand[j] = players[i]->getHand()[j];
					}
					players[i]->setHand(tempHand);
				}
				break;
		}
	}
	sleep(2);
	clearBottomBanner();
	refreshDisplay();
	displayCards();
	passDirection = (passDirection + 1) % 4;
}

void Game::determineStartingLead()
{
	for(int i = 0; i < 4; i++)
		if(players[i]->getHand()[0] == 1)	//if the first card in the hand is the 2 of clubs, player is now lead
			leadPlayer = i;
}

void Game::resolveTrick()
{
	std::ostringstream oss;
	bool heartsBroken = rules->getHearts();
	for(int i = 0; i < 4; i++)
	{
		int* inputCard = new int;
		if((i + leadPlayer)%4 == 0)
		{	
			int tries = 1;
			int inputIndex;
			do
			{
				inputIndex = getInput();
				*inputCard = players[0]->peekCard(inputIndex);
				oss.str("");
				string* cardSuitAndNum = cardData(*inputCard);
				string suit = "";
				switch(rules->suit)
				{
					case 0:
						suit = "Clubs";
						break;
					case 1:
						suit = "Diamonds";
						break;
					case 2:
						suit = "Spades";
						break;
					case 3:
						suit = "Hearts";
						break;
				}
				oss << "Incorrect choice # " << tries << " | Current suit: " << suit << " | Suit attempted: " << cardSuitAndNum[0] << " | Card attempted: " << cardSuitAndNum[1] << " | Error: ";
				string error = rules->testCard(players[0], inputCard);
				if(error == "")
					break;
				oss << error;
				displayInBottomBanner(oss.str());
				tries++;
			}while(true);
			int* playedCard = new int;
			*playedCard = players[0]->popCard(inputIndex);
			rules->playCard(playedCard);
			delete inputCard;
		}
		else
		{
			static_cast<AI*>(players[(i+leadPlayer)%4])->setVariables(i, rules->suit);
			*inputCard = static_cast<AI*>(players[(i + leadPlayer)%4])->chooseCard();
			rules->playCard(inputCard);
		}
		
		refreshDisplay();
		displayCards();
	}

	int* trickScore = new int[4];
	trickScore = rules->scoreTrick();
	for(int i = 0; i < 4; i++)
		players[(i + leadPlayer)%4]->addRoundScore(trickScore[i]);
	delete trickScore;

	leadPlayer = (rules->getTrickTaker() + leadPlayer) % 4;

	oss.str("");
	oss << "Trick taken by Player " << leadPlayer + 1 << " ";
	switch(leadPlayer)
	{
		case 0:
			oss <<"(You)";
			break;
		case 1:
			oss <<"(Left)";
			break;
		case 2:
			oss <<"(Across)";
			break;
		case 3:
			oss <<"(Right)";
			break;
	} 
	if(!heartsBroken && rules->getHearts())
		oss << " | Hearts is Broken!";
	displayInBottomBanner(oss.str());

	rules->resetTrick();
	rules->setFirstTrick(false);
	sleep(3);
	refreshDisplay();
	displayCards();
}

int Game::getInput()
{
	while(true)
	{
		char key = gameDisplay->captureInput();
		if(key == -1)
		{
			int mouseX = gameDisplay->getMouseEventX() - ((gameDisplay->getCols()/2) - 6*6 - 3);
			int mouseY = gameDisplay->getMouseEventY() - (gameDisplay->getLines() - 10);
			if (gameDisplay->getMouseEventButton()&LEFT_CLICK)
			{
				if(mouseY <= 5 && mouseY >= 0 && mouseX < 6*13 && mouseX >= 0 && players[0]->peekCard(mouseX/6) != 0)
					return mouseX/6;
			}
			else if(gameDisplay->getMouseEventButton()&RIGHT_CLICK)
			{
				std::ostringstream oss;
				oss.str("");
				oss << "Right click is an invalid input.";
				displayInBottomBanner(oss.str());
			}
			else if(gameDisplay->getMouseEventButton()&LEFT_DOWN)
			{
				std::ostringstream oss;
				oss.str("");
				oss << "Mouse dragging is an invalid input.";
				displayInBottomBanner(oss.str());
			}
		} else if(key > 0)
		{
			std::ostringstream oss;
			oss.str("");
			oss << "Keyboard key is an invalid input.";
			displayInBottomBanner(oss.str());
			
		}
	}
}

void Game::refreshDisplay()
{	
	gameDisplay->eraseBox(0,1,gameDisplay->getCols(),gameDisplay->getLines()-2);
}

void Game::displayScores()
{
	std::ostringstream oss;
	oss << "HEARTS | Left click to select cards | Scores: ";
	for(int i = 0; i < 4; i++)
	{
		switch(i)
		{
			case 0:
				oss << "Player " << (i+1) << " (You): " << scores[i] << "     ";
				break;
			case 1:
				oss << "Player " << (i+1) << " (Left): " << scores[i] << "     ";
				break;
			case 2:
				oss << "Player " << (i+1) << " (Across): " << scores[i] << "     ";
				break;
			case 3:
				oss << "Player " << (i+1) << " (Right): " << scores[i] << "     ";
				break;
		}
	}
	displayInTopBanner(oss.str());
}

void Game::clearTopBanner()
{
	gameDisplay->eraseBox(0,0,gameDisplay->getCols(),1);
}

void Game::displayInTopBanner(string input)
{
	clearTopBanner();
	gameDisplay->bannerTop(input);
	gameDisplay->updateScreen();
}

void Game::clearBottomBanner()
{
	gameDisplay->eraseBox(0,gameDisplay->getLines()-1,gameDisplay->getCols(),1);
}

void Game::displayInBottomBanner(string input)
{
	clearBottomBanner();
	gameDisplay->bannerBottom(input);
	gameDisplay->updateScreen();
}

void Game::displayCards()
{
	for(int i = 0; i < 13; i++)
	{
		int* currentPlayerCard = adjustCardForDisplay(players[0]->getHand()[i]);
		if(players[0]->getHand()[i] != 0)
			gameDisplay->displayCard(((gameDisplay->getCols()/2) - 6*6 - 3) + i*6, gameDisplay->getLines() - 10, currentPlayerCard[0], currentPlayerCard[1], A_BOLD);
		currentPlayerCard = adjustCardForDisplay(players[1]->getHand()[i]);
		if(players[1]->getHand()[i] != 0)
			gameDisplay->displayCard(10, ((gameDisplay->getLines()/2) - 6*2 - 3) + i*2, 0, 0, A_BOLD);
		currentPlayerCard = adjustCardForDisplay(players[2]->getHand()[i]);
		if(players[2]->getHand()[i] != 0)
			gameDisplay->displayCard(((gameDisplay->getCols()/2) - 6*6 - 3) + i*6, 5, 0, 0, A_BOLD);
		currentPlayerCard = adjustCardForDisplay(players[3]->getHand()[i]);
		if(players[3]->getHand()[i] != 0)
			gameDisplay->displayCard(gameDisplay->getCols() - 16, ((gameDisplay->getLines()/2) - 6*2 - 3) + i*2, 0, 0, A_BOLD);
		delete [] currentPlayerCard;
	}

	for(int i = 0; i < 4; i++)
	{
		if(rules->getTrick()[i] != 0)
		{
			int* currentCard = adjustCardForDisplay(rules->getTrick()[i]);
			switch((i + leadPlayer) % 4)
			{
				case 0:
					gameDisplay->displayCard((gameDisplay->getCols()/2) - 3, (gameDisplay->getLines()/2) + 3, currentCard[0], currentCard[1], A_BOLD);
					break;
				case 1:
					gameDisplay->displayCard((gameDisplay->getCols()/2) - 9, (gameDisplay->getLines()/2) - 3, currentCard[0], currentCard[1], A_BOLD);
					break;
				case 2:
					gameDisplay->displayCard((gameDisplay->getCols()/2) - 3, (gameDisplay->getLines()/2) - 8, currentCard[0], currentCard[1], A_BOLD);
					break;
				case 3:
					gameDisplay->displayCard((gameDisplay->getCols()/2) + 3, (gameDisplay->getLines()/2) - 3, currentCard[0], currentCard[1], A_BOLD);
					break;
			}
			delete [] currentCard;
		}
	}
	
	gameDisplay->updateScreen();
}

int* Game::adjustCardForDisplay(int card)
{
	int* cardProps = new int[2];
	cardProps[0] = ((card-1)/13 + 2) % 4 + 1;
	int cardTemp = card;
	if(cardTemp%13 == 0)
		cardTemp -= 12;
	else
		cardTemp++;
	cardTemp = (cardTemp-1)%13 + 1;
	cardProps[1] = cardTemp;
	return cardProps;
}

string* Game::cardData(int card)
{
	string* cardSuitAndNum = new string[2];
	switch((card-1)/13)
	{
		case 0:
			cardSuitAndNum[0] = "Clubs";
			break;
		case 1:
			cardSuitAndNum[0] = "Diamonds";
			break;
		case 2:
			cardSuitAndNum[0] = "Spades";
			break;
		case 3:
			cardSuitAndNum[0] = "Hearts";
			break;
	}
	switch((card-1)%13)
	{
		case 0:
			cardSuitAndNum[1] = "Two";
			break;
		case 1:
			cardSuitAndNum[1] = "Three";
			break;
		case 2:
			cardSuitAndNum[1] = "Four";
			break;
		case 3:
			cardSuitAndNum[1] = "Five";
			break;
		case 4:
			cardSuitAndNum[1] = "Six";
			break;
		case 5:
			cardSuitAndNum[1] = "Seven";
			break;
		case 6:
			cardSuitAndNum[1] = "Eight";
			break;
		case 7:
			cardSuitAndNum[1] = "Nine";
			break;
		case 8:
			cardSuitAndNum[1] = "Ten";
			break;
		case 9:
			cardSuitAndNum[1] = "Jack";
			break;
		case 10:
			cardSuitAndNum[1] = "Queen";
			break;
		case 11:
			cardSuitAndNum[1] = "King";
			break;
		case 12:
			cardSuitAndNum[1] = "Ace";
			break;
	}
	return cardSuitAndNum;
}

void Game::endRound()
{
	int* playerScores = new int[4];
	for(int i = 0; i < 4; i++)
	{
		playerScores[i] = players[i]->getRoundScore();
		players[i]->resetRoundScore();
	}
	addToScores(playerScores);
	displayScores();

	rules->resetHearts();
	rules->resetTwoPlayed();
	rules->setFirstTrick(true);

	delete [] playerScores;
}

int Game::checkMoon(int roundScores[4])
{
	for(int i = 0; i < 4; i++)
		if(roundScores[i] == 26)
			return i;
	return -1;
}

bool Game::checkScores()
{
	for(int i = 0; i < 4; i++)
		if(scores[i] >= 100)
			return true;
	return false;
}

int* Game::getScores()
{
	return scores;
}

void Game::endGame()
{
	system("reset");
	std::ostringstream oss;
	oss << "Final Score: ";
	for(int i = 0; i < 4; i++)
		oss << "Player " << (i+1) << " Score: " << scores[i] << "     ";
	oss << std::endl << "~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~" << std::endl << "Winner(s): ";
	int lowestScore = scores[0];
	for(int i = 1; i < 4; i++)
		if(scores[i] < lowestScore)
			lowestScore = scores[i];
	for(int i = 0; i < 4; i++)
		if(scores[i] == lowestScore)
			oss << "Player " << i+1 << " ";
	oss << " -CONGRATULATIONS-";
	std::cout << oss.str() << std::endl;
}

void Game::startGame()
{
	while(playGame());
}

void Game::addToScores(int roundScores[4])
{
	int player = checkMoon(roundScores);
	if(player != -1)
	{
		for(int i = 0; i < 4; i++)
		{
			if(player != i)
				scores[i] += 26;
		}
	}
	else
	{
		for(int i = 0; i < 4; i++)
			scores[i] += roundScores[i];
	}
}

void Game::printScores()
{
	for(int i = 0; i < 4; i++)
		std::cout << scores[i] << ' ';
	std::cout << std::endl;
}

void Game::resetScores()
{
	for(int i = 0; i < 4; i++)
		scores[i] = 0;
}
