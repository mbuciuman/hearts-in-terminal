// AI Player Class

#include <iostream>
#include "player.hpp"

AI::AI()
: Player()
{	
}

AI::AI(int h[13])
: Player(h)
{
}

int AI::chooseCard()
{
	for(int i = 0; i < 13; i++)
	{
		if(getHand()[i] != 0) 						//if not a nonexistant card
			if(turnNumber == 0 || (getHand()[i]-1)/13 == suit) 		//checks conditions for card
				return popCard(i);
	}
	for(int i = 0; i < 13; i++)
	{	
		if(getHand()[i] != 0)
			return popCard(i);
	}
	return -1; //should not happen!
}

AI::~AI()
{
	std::cout << "AI::~AI" << std::endl;
}

int AI::lowestCardInSuit(int s)
{
	std::cout << "AI::lowestCardInSuit - s = " << s << std::endl;
	return 0;
}

int AI::highestCardInSuit(int s)
{
	std::cout << "AI::highestCardInSuit - s = " << s << std::endl;
	return 0;
}

void AI::setVariables(int turnN, int s)
{
	turnNumber = turnN;
	suit = s;
}
