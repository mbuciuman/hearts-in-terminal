// Deck of Cards Management Class

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include "deck.hpp"

Deck::Deck()
{
	newOrderedDeck();
	newShuffle();
}

Deck::~Deck()
{
	std::cout << "Deck::~Deck" << std::endl;
}

int* Deck::getDeck()
{
	return deck;
}

void Deck::swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

void Deck::newOrderedDeck()
{
	// Build a new deck of cards in order
	for(int i = 0; i<52; i++){
		deck[i] = i+1;
	}
}

void Deck::newShuffle()
{
	// Build a new deck of cards shuffled.
	// Start by building new in order deck.
	for(int i = 0; i<52; i++)
	{
		deck[i] = i+1;
	}
	
	// Seed random with current time. 
	srand(time(NULL));
	
	// Perform random swaps on deck. 
	for(int i = 51; i > 0; i--)
	{
		int j = rand() % (i+1);
		swap(&deck[i], &deck[j]);
	}
}
